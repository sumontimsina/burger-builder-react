import React from 'react';
import classes from './Burger.module.css';
import BurgerIngrediants from './BurgerIngredients/BurgerIngrediants';

const burger =( props) =>{
    // console.log(props);
    let transformedIngrediants = Object.keys(props.ingrediants)
    .map(igKey=> {
        return[...Array(props.ingrediants[igKey])].map((_ , i) =>{
            return <BurgerIngrediants key={igKey + i} type = {igKey}/>
        });
    })
    .reduce((arr, el) =>{
        return arr.concat(el)
    },[]);
    if(transformedIngrediants.length === 0){
        transformedIngrediants= <p>Please start adding Ingrediants</p>
    }    
    return(
        <div className={classes.Burger}>
            <BurgerIngrediants type='bread-top'/>
            {transformedIngrediants}
            <BurgerIngrediants type='bread-bottom'/>
        </div>
    );
}


export default burger;