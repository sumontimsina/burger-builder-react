import React ,{Component} from 'react';
import { connect } from 'react-redux';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import {Route} from 'react-router-dom';
import ContactData from './ContactData/ContactData';

class Checkout extends Component{

    checkoutCancelledHandler= ()=>{
        this.props.history.goBack();
    };
    cceckoutContinuedHandler= ()=>{
        this.props.history.replace('/checkout/contact-data');

    };
    render(){
        return(
            <div>
                <CheckoutSummary 
                    ingrediants = {this.props.ings}
                    checkoutCancelled={this.checkoutCancelledHandler}
                    cceckoutContinued={this.cceckoutContinuedHandler}/>
                <Route 
                    path={this.props.match.path + '/contact-data'} 
                    component={ContactData}/>
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return{
        ings:state.ingrediants,
        price: state.totalPrice
    };
}


export default connect(mapStateToProps)(Checkout);